# Python Tutorials

This repository contains some code snippets for some basic algorithms and techniques. 

### Getting Started
To start the server : 
```console
	$ cd python-tutorials
	$ jupyter notebook 
```
This will open a new tab in your  browser and then you can start your work.
To exit, press **Ctrl + c** in the command line/ prompt. 

### Author

* [Pranjal Gupta](https://gitlab.com/PranjalGupta2199)